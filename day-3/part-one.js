import fs from "fs";

const input = fs
  .readFileSync("./input.txt")
  .toString()
  .split("\n")
  .map((x) => x.split(""));

const isNumber = (char) => !isNaN(parseInt(char));

const isSpecialChar = (char) => !isNumber(char) && char !== ".";

const dirs = [
  [-1, -1],
  [0, -1],
  [1, -1],
  [-1, 0],
  [1, 0],
  [-1, 1],
  [0, 1],
  [1, 1],
];

const get = (i, j, [y, x] = [0, 0]) => {
  const row = input[i + y];
  return row ? row[j + x] : undefined;
};

let sum = 0;

for (let y = 0; y < input.length; ++y) {
  const row = input[y];
  let isNumberChar = false;
  let isPartNumber = false;
  let currentNumber = "";

  for (let x = 0; x < row.length; ++x) {
    isNumberChar = isNumber(get(y, x));

    if (!isNumberChar && isPartNumber) {
      sum += parseInt(currentNumber);
    }

    if (!isNumberChar) {
      currentNumber = "";
      isPartNumber = false;
    }

    if (isNumberChar && !isPartNumber) {
      const isPartNumberChar = dirs.reduce((acc, dir) => {
        const char = get(y, x, dir);
        return acc || (char !== undefined && isSpecialChar(char));
      }, false);

      if (isPartNumberChar) {
        isPartNumber = true;
      }
    }

    if (isNumberChar) {
      currentNumber += get(y, x);
    }
  }

  if (isNumberChar && isPartNumber) {
    sum += parseInt(currentNumber);
  }
}

console.log(sum);
