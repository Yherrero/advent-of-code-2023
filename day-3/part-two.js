import fs from "fs";

const input = fs
  .readFileSync("./input.txt")
  .toString()
  .split("\n")
  .map((x) => x.split(""));

const isNumber = (char) => !isNaN(parseInt(char));

const isGear = (char) => char === "*";

const dirs = [
  [-1, -1],
  [0, -1],
  [1, -1],
  [-1, 0],
  [1, 0],
  [-1, 1],
  [0, 1],
  [1, 1],
];

const get = (i, j, [y, x] = [0, 0]) => {
  const row = input[i + y];
  return row ? row[j + x] : undefined;
};

const goods = [];

for (let y = 0; y < input.length; ++y) {
  const row = input[y];
  let isNumberChar = false;
  let currentNumber = "";
  let surroundingGears = [];

  for (let x = 0; x < row.length; ++x) {
    isNumberChar = isNumber(get(y, x));

    if (!isNumberChar && surroundingGears.length > 0) {
      goods.push({
        number: parseInt(currentNumber),
        surroundingGears,
      });
    }

    if (!isNumberChar) {
      currentNumber = "";
      surroundingGears = [];
    }

    if (isNumberChar) {
      dirs.forEach(([dy, dx]) => {
        const char = get(y, x, [dy, dx]);
        if (char && isGear(char)) {
          const gearKey = `${x + dx}-${y + dy}`;
          !surroundingGears.includes(gearKey) && surroundingGears.push(gearKey);
        }
      });
    }

    if (isNumberChar) {
      currentNumber += get(y, x);
    }
  }

  if (isNumberChar && surroundingGears.length > 0) {
    goods.push({
      number: parseInt(currentNumber),
      surroundingGears,
    });
  }
}

const gearList = goods.reduce(
  (list, currentNumber) =>
    currentNumber.surroundingGears.reduce(
      (newList, gearKey) => ({
        ...newList,
        [gearKey]: newList[gearKey]
          ? [...newList[gearKey], currentNumber.number]
          : [currentNumber.number],
      }),
      list
    ),
  {}
);

const sum = Object.values(gearList).reduce(
  (result, gear) =>
    gear.length === 2
      ? result + gear.reduce((power, value) => power * value, 1)
      : result,
  0
);

console.log(sum);
