import fs from "fs";

export const readFile = (file) => {
  const input = fs.readFileSync(file).toString();

  return input.split("\n");
};
