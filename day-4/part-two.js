import fs from "fs";

const input = fs
  .readFileSync("./input.txt")
  .toString()
  .split("\n")
  .filter((v) => v);

const getScore = (card) => {
  const matches = card.match(/: (.+?) \| (.+)/);
  const winning = matches[1].match(/\d+/g);
  const numbers = matches[2].match(/\d+/g);
  const winningDict = winning.reduce(
    (dict, wNumber) => ({ ...dict, [wNumber]: true }),
    {}
  );

  return numbers.filter((v) => v && winningDict[v]).length;
};

let cards = Array(input.length).fill(1);

input.forEach((card, i) => {
  const score = getScore(card);
  if (score) {
    for (let j = i + 1; j <= i + score; j++) {
      if (cards[j]) cards[j] += cards[i];
    }
  }
});

const result = cards.reduce((a, b) => a + b, 0);

console.log(result);
