import fs from "fs";

const input = fs
  .readFileSync("./input.txt")
  .toString()
  .split("\n")
  .filter((v) => v);

const getScore = (card) => {
  const matches = card.match(/: (.+?) \| (.+)/);
  const winning = matches[1].match(/\d+/g);
  const numbers = matches[2].match(/\d+/g);
  const winningDict = winning.reduce(
    (dict, wNumber) => ({ ...dict, [wNumber]: true }),
    {}
  );

  return numbers.filter((v) => v && winningDict[v]).length;
};

const sum = input.reduce((result, card) => {
  const score = getScore(card);
  return score === 0 ? result + score : result + (1 << (score - 1));
}, 0);

console.log(sum);
