import fs from "fs";

const numberMapping = {
  one: "one1one",
  two: "two2two",
  three: "three3three",
  four: "four4four",
  five: "five5five",
  six: "six6six",
  seven: "seven7seven",
  eight: "eight8eight",
  nine: "nine9nine",
};

const partTwo = (file) => {
  let input = fs.readFileSync(file).toString();
  Object.keys(numberMapping).forEach(
    (number) => (input = input.replaceAll(number, numberMapping[number]))
  );

  return input.split("\n").reduce((result, currentLine) => {
    const numbers = currentLine.split("").filter((el) => !isNaN(+el));
    return numbers.length === 0
      ? result
      : result + parseInt(`${numbers[0]}${numbers[numbers.length - 1]}`);
  }, 0);
};

console.log(partTwo("./input.txt"));
