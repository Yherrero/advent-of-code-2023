import fs from "fs";

const partOne = (file) => {
  const input = fs.readFileSync(file).toString();

  return input.split("\n").reduce((result, currentLine) => {
    const numbers = currentLine.split("").filter((el) => !isNaN(+el));
    return numbers.length === 0
      ? result
      : result + parseInt(`${numbers[0]}${numbers[numbers.length - 1]}`);
  }, 0);
};

console.log(partOne("./input.txt"));
