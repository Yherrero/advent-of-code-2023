import fs from "fs";

const partOne = (file) => {
  const input = fs.readFileSync(file).toString();

  const gameLimits = {
    red: 12,
    green: 13,
    blue: 14,
  };

  return input.split("\n").reduce((result, line) => {
    const [gameName, gameSets] = line.split(":");
    const gameId = gameName.split(" ")[1];
    const sets = gameSets.split(";");

    const cubeRequirements = sets.reduce(
      (requirements, set) => {
        const currentRequirements = { ...requirements };

        set.split(", ").forEach((cubeSet) => {
          const [value, key] = cubeSet.trim().split(" ");
          if (currentRequirements[key] < +value) {
            currentRequirements[key] = +value;
          }
        });

        return currentRequirements;
      },
      {
        red: 0,
        green: 0,
        blue: 0,
      }
    );

    return Object.keys(gameLimits).every(
      (color) => gameLimits[color] >= cubeRequirements[color]
    )
      ? result + +gameId
      : result;
  }, 0);
};

console.log(partOne("./input.txt"));
