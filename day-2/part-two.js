import fs from "fs";

const partTwo = (file) => {
  const input = fs.readFileSync(file).toString();

  return input.split("\n").reduce((result, line) => {
    const [_, gameSets] = line.split(":");
    const sets = gameSets.split(";");

    const cubeRequirements = sets.reduce(
      (requirements, set) => {
        const currentRequirements = { ...requirements };

        set.split(", ").forEach((cubeSet) => {
          const [value, key] = cubeSet.trim().split(" ");
          if (currentRequirements[key] < +value) {
            currentRequirements[key] = +value;
          }
        });

        return currentRequirements;
      },
      {
        red: 0,
        green: 0,
        blue: 0,
      }
    );

    return (
      result +
      cubeRequirements.red * cubeRequirements.green * cubeRequirements.blue
    );
  }, 0);
};

console.log(partTwo("./input.txt"));
